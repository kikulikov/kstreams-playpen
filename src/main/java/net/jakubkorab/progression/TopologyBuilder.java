package net.jakubkorab.progression;

import org.apache.kafka.streams.Topology;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public interface TopologyBuilder {
    Topology build();
}

package net.jakubkorab.ksia.chapter03;

import net.jakubkorab.ksia.model.Purchase;
import net.jakubkorab.ksia.model.PurchasePattern;
import net.jakubkorab.ksia.model.RewardAccumulator;
import net.jakubkorab.ksia.util.serializer.JsonDeserializer;
import net.jakubkorab.ksia.util.serializer.JsonSerializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Printed;
import org.apache.kafka.streams.kstream.Produced;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;
import java.util.concurrent.CountDownLatch;

@SuppressWarnings("Duplicates")
public class ZMartApp {
    private static Logger LOG = LoggerFactory.getLogger(ZMartApp.class);

    public static void main(String[] args) throws InterruptedException {
        // build up the config
        Properties props = new Properties();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "zmart-app");
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");

        // Construct the serde instances
        Serde<String> stringSerde = Serdes.String();
        Serde<Purchase> purchaseSerde =
                Serdes.serdeFrom(new JsonSerializer<>(), new JsonDeserializer<>(Purchase.class));
        Serde<PurchasePattern> purchasePatternSerde =
                Serdes.serdeFrom(new JsonSerializer<>(), new JsonDeserializer<>(PurchasePattern.class));
        Serde<RewardAccumulator> rewardAccumulatorSerde =
                Serdes.serdeFrom(new JsonSerializer<>(), new JsonDeserializer<>(RewardAccumulator.class));

        // construct the processor topology
        StreamsBuilder builder = new StreamsBuilder();
        KStream<String, Purchase> purchaseKStream = builder.stream("transactions", Consumed.with(stringSerde, purchaseSerde))
                .mapValues(purchase -> Purchase.builder(purchase).maskCreditCard().build());

        // split into 3 fragments
        KStream<String, PurchasePattern> purchasePatternKStream = purchaseKStream.mapValues(purchase -> PurchasePattern.builder(purchase).build());
        purchasePatternKStream.print(Printed.<String, PurchasePattern>toSysOut().withLabel("patterns"));
        purchasePatternKStream.to("patterns", Produced.with(stringSerde, purchasePatternSerde));

        purchaseKStream.mapValues(purchase -> RewardAccumulator.builder(purchase).build())
                .peek((key, val) -> LOG.info("patterns: {} -> {}", key, val))
                .to("rewards", Produced.with(stringSerde, rewardAccumulatorSerde));

        purchaseKStream.print(Printed.<String, Purchase>toSysOut().withLabel("purchases"));
        purchaseKStream.to("purchases", Produced.with(stringSerde, purchaseSerde));

        Topology topology = builder.build();

        // start the streams app
        KafkaStreams kafkaStreams = new KafkaStreams(topology, props);
        kafkaStreams.start();

        CountDownLatch latch = new CountDownLatch(1);
        Runtime.getRuntime().addShutdownHook(new Thread(latch::countDown));
        latch.await();

        LOG.info("Shutting down app now");
        kafkaStreams.close();
    }
}

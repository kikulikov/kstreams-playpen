package net.jakubkorab.ksia.chapter04;

import net.jakubkorab.ksia.model.Purchase;
import net.jakubkorab.ksia.model.RewardAccumulator;
import org.apache.commons.lang3.Validate;
import org.apache.kafka.streams.kstream.ValueTransformer;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.apache.kafka.streams.state.KeyValueStore;

public class PurchaseRewardTransformer implements ValueTransformer<Purchase, RewardAccumulator> {

    private final String storeName;

    private ProcessorContext context;
    private KeyValueStore<String, Integer> stateStore; // maps customerId to accumulated reward points

    public PurchaseRewardTransformer(String storeName) {
        Validate.notEmpty(storeName);
        this.storeName = storeName;
    }

    @Override
    public void init(ProcessorContext context) {
        this.context = context;
        this.stateStore = (KeyValueStore) context.getStateStore(storeName);
        Validate.notNull(stateStore);
    }

    @Override
    public RewardAccumulator transform(Purchase purchase) {
        RewardAccumulator rewardAccumulator = RewardAccumulator.builder(purchase).build();
        String customerId = rewardAccumulator.getCustomerId();
        Integer accumulatedSoFar = stateStore.get(customerId);
        if (accumulatedSoFar != null) {
            rewardAccumulator.addRewardPoints(accumulatedSoFar);
        }
        stateStore.put(customerId, rewardAccumulator.getTotalRewardPoints());
        return rewardAccumulator;
    }

    @Override
    public void close() {

    }
}

package net.jakubkorab.ksia;

import com.github.javafaker.Beer;
import com.github.javafaker.Faker;
import com.github.javafaker.Name;
import net.jakubkorab.ksia.model.Purchase;
import net.jakubkorab.ksia.util.serializer.JsonSerializer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Properties;

public class PurchaseDataGenerator {

    private final static Logger LOG = LoggerFactory.getLogger(PurchaseDataGenerator.class);

    public static final int CUSTOMER_COUNT = 20;
    public static final int PURCHASE_COUNT = 100;

    public static void main(String[] args) {

        JsonSerializer<Purchase> serializer = new JsonSerializer<>();

        Faker faker = Faker.instance();

        // Pregenerate a bunch of consumers so we get consistent names and ids for purchases
        // Names won't be consistent between runs
        Customer[] customers = generateCustomers(faker, CUSTOMER_COUNT);

        Properties config = new Properties();
        config.setProperty("bootstrap.servers", "localhost:9092");
        config.setProperty("key.serializer", StringSerializer.class.getName());
        config.setProperty("value.serializer", JsonSerializer.class.getName());
        config.setProperty("acks", "1");

        KafkaProducer<String, Purchase> producer = new KafkaProducer<>(config);

        Instant purchaseTime = Instant.now().minus(1, ChronoUnit.DAYS);

        for (int i = 0; i < PURCHASE_COUNT; i++) {
            Beer beer = faker.beer();

            Customer customer = customers[faker.number().numberBetween(0, CUSTOMER_COUNT - 1)];

            Purchase purchase = Purchase.builder()
                    .customerId(customer.id.toString())
                    .firstName(customer.firstName)
                    .lastName(customer.lastName)
                    .purchaseDate(new Date(purchaseTime.toEpochMilli()))
                    .department("beer")
                    .itemPurchased(beer.name())
                    .category(beer.style())
                    .quanity(faker.number().numberBetween(1, 12))
                    .price(faker.number().randomDouble(2,1,10))
                    .employeeId(faker.number().numberBetween(1, 10))
                    .creditCardNumber(creditCardNumber(faker)).build();

            purchaseTime = purchaseTime.plusSeconds(faker.number().numberBetween(5, 20));

            byte[] bytes = serializer.serialize("foo", purchase);
            LOG.info(new String(bytes, StandardCharsets.UTF_8));
            producer.send(new ProducerRecord<String, Purchase>("transactions", null, purchase));
        }

        producer.close();
    }

    private static Customer[] generateCustomers(Faker faker, int customerCount) {
        Customer[] customers = new Customer[customerCount];
        for (int i = 0; i < customerCount; i++) {
            Name name = faker.name();
            customers[i] = new Customer(i + 1, name.firstName(), name.lastName());
        }
        return customers;
    }

    private static String creditCardNumber(Faker faker) {
        return faker.number().digits(4) + "-" +
                faker.number().digits(4) + "-" +
                faker.number().digits(4) + "-" +
                faker.number().digits(4);
    }

    static class Customer {
        final Integer id;
        final String firstName;
        final String lastName;

        public Customer(Integer id, String firstName, String lastName) {
            this.id = id;
            this.firstName = firstName;
            this.lastName = lastName;
        }
    }
}

package net.jakubkorab.ksia.chapter03;

import net.jakubkorab.ksia.model.Purchase;
import net.jakubkorab.ksia.model.PurchasePattern;
import net.jakubkorab.ksia.model.RewardAccumulator;
import net.jakubkorab.ksia.util.serializer.JsonDeserializer;
import net.jakubkorab.ksia.util.serializer.JsonSerializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Predicate;
import org.apache.kafka.streams.kstream.Produced;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;
import java.util.concurrent.CountDownLatch;

@SuppressWarnings("Duplicates")
public class ZMartAdvReqtsApp {
    private static Logger LOG = LoggerFactory.getLogger(ZMartAdvReqtsApp.class);

    private enum BeerStyle {
        Ale((key, purchase) -> purchase.getCategory().toLowerCase().contains("ale")),
        Lager((key, purchase) -> purchase.getCategory().toLowerCase().contains("lager"));

        final Predicate<String, Purchase> predicate;

        BeerStyle(Predicate<String, Purchase> predicate) {
            this.predicate = predicate;
        }
    }

    public static void main(String[] args) throws InterruptedException {
        // build up the config
        Properties props = new Properties();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "zmart-app");
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");

        // Construct the serde instances
        Serde<String> stringSerde = Serdes.String();
        Serde<Purchase> purchaseSerde =
                Serdes.serdeFrom(new JsonSerializer<>(), new JsonDeserializer<>(Purchase.class));
        Serde<PurchasePattern> purchasePatternSerde =
                Serdes.serdeFrom(new JsonSerializer<>(), new JsonDeserializer<>(PurchasePattern.class));
        Serde<RewardAccumulator> rewardAccumulatorSerde =
                Serdes.serdeFrom(new JsonSerializer<>(), new JsonDeserializer<>(RewardAccumulator.class));

        // construct the processor topology
        StreamsBuilder builder = new StreamsBuilder();
        KStream<String, Purchase> purchaseKStream = builder.stream("transactions", Consumed.with(stringSerde, purchaseSerde))
                .mapValues(purchase -> Purchase.builder(purchase).maskCreditCard().build());

        // split into 3 fragments
        purchaseKStream.mapValues(purchase -> PurchasePattern.builder(purchase).build())
                .peek((key, val) -> LOG.info("patterns: {} -> {}", key, val))
                .to("patterns", Produced.with(stringSerde, purchasePatternSerde));

        purchaseKStream.mapValues(purchase -> RewardAccumulator.builder(purchase).build())
                .peek((key, val) -> LOG.info("rewards: {} -> {}", key, val))
                .to("rewards", Produced.with(stringSerde, rewardAccumulatorSerde));

        purchaseKStream.peek((key, val) -> LOG.info("purchases: {} -> {}", key, val))
                .filter((key, purchase) -> purchase.getPrice() > 5) // things that are true
                .selectKey((key, purchase) -> purchase.getPurchaseDate().getTime())
                .to("purchases", Produced.with(Serdes.Long(), purchaseSerde));

        purchaseKStream
                .filter((key, purchase) -> {
                    Integer employeeId = purchase.getEmployeeId();
                    return (employeeId != null) && (employeeId == 9);
                }) // things that are true
                .peek((key, val) -> LOG.info("dodgyEmployee: {} -> {}", key, val))
                .foreach((key, purchase) -> LOG.info("Inserting order by employee {} into DB for inspection", purchase.getEmployeeId()));

        KStream<String, Purchase>[] streamByStyle = purchaseKStream.branch(BeerStyle.Ale.predicate, BeerStyle.Lager.predicate);
        streamByStyle[BeerStyle.Ale.ordinal()]
                .peek((key, val) -> LOG.info("ales: {} -> {}", key, val))
                .to("ales", Produced.with(stringSerde, purchaseSerde));

        streamByStyle[BeerStyle.Lager.ordinal()]
                .peek((key, val) -> LOG.info("lagers: {} -> {}", key, val))
                .to("lagers", Produced.with(stringSerde, purchaseSerde));

        Topology topology = builder.build();

        // start the streams app
        KafkaStreams kafkaStreams = new KafkaStreams(topology, props);
        kafkaStreams.start();

        CountDownLatch latch = new CountDownLatch(1);
        Runtime.getRuntime().addShutdownHook(new Thread(latch::countDown));
        latch.await();

        LOG.info("Shutting down app now");
        kafkaStreams.close();
    }
}
